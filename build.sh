#!/bin/sh

script="$0"
path="$(dirname $(realpath "$script"))"
dist="$path/dist"
site="$path/site"

[ ! -d "$dist" ] && mkdir "$dist"

shblg -i "$site" -o "$dist"
