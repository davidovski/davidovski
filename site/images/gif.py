#!/usr/bin/env python
import glob
import sys
import os
import math
import random
from PIL import Image


def color(hex_value):
    h = hex_value.lstrip('#')
    while len(h) < 8:
        h += "f" 
    return tuple(int(h[i:i+2], 16) for i in (0, 2, 4, 6))

def rgb_to_v(c):
    r, g, b = c[0]/255.0, c[1]/255.0, c[2]/255.0
    mx = max(r, g, b)
    v = mx*100
    return v

replace = color("#f58f44")
colors = [
        color("#191919"),
        color("#282a2e"),
        ]

def make(colors, inp=None):
    p = len(colors)
    w = int(128 / p) * p
    h = int(128 / p) * p

    frames = []

    for i in range(int(p*1*math.pi)):
        if inp is None:
            image = Image.new("RGBA", (w, h), colors[0])
        else:
            image = Image.open(inp).convert("RGBA")

        for x in range(image.width):
            for y in range(image.height):
                f = 2
                r = random.randint(-f, f)
                z = (i) - (y/(p/4)) + r
                v = math.floor( (math.sin(z) + 1) * len(colors) * 0.5)
                c = colors[v]
                if inp is not None:
                    existing = image.getpixel((x, y)) 
                    if existing[:3] == replace[:3]:
                        image.putpixel((x,y), c)
                else:
                    image.putpixel((x,y), c)

        frames.append(image.convert("P"))


    frames[0].save(sys.stdout, mode="P", format="GIF", append_images=frames[1:], save_all=True, duration=50, loop=0)


template = None
if len(sys.argv) > 1 and os.path.exists(sys.argv[1]):
    template = sys.argv[1]
    colors = colors + [ color("#fefefe") ]

sorted(colors, key=rgb_to_v)
make(colors, inp=template)
#make("dist/images/remotecontrol.gif", colors2, inp="images/remotecontrol-small.png")


