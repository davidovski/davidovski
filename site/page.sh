#!/bin/sh

# if this is called with -s then make it the small version of the page

type="regular-window"
[ "$1" = "-s" ] &&{
    type="small-window"
    shift
}

cat << EOF
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
		<link rel="stylesheet" href="/style.css">
		<title>davidovski.xyz</title>
	</head>
	<body bgcolor="#191919" text="#f58f44" link="#b4d6d1">
		<div valign="middle" class="main $type" bgcolor="#191919" text="#f58f44">
			<div class="header">
				<a href="/"<h1 class="title">davidovski.xyz</h1></a>
				<hr>
				<div class="links">
					<a href="/git">git</a>
					|| 
					<a color="#cc6666" style="color: var(--red);" href="/m">m</a> 
					|
					<a color="#cc6666" style="color: var(--red);" href="/f">f</a>
					|
					<a color="#cc6666" style="color: var(--red);" href="/s">s</a> 
					|
					<a color="#cc6666" style="color: var(--red);" href="/x">x</a> 
					||
					<a color="#b5bd68" style="color: var(--green)" href="http://chat.davidovski.xyz/">chat</a>
					|
					<a color="#b5bd68" style="color: var(--green)" href="mailto:david@davidovski.xyz">mail</a> 
					| 
					<a color="#b5bd68" style="color: var(--green)" href="/rss.xml">rss</a>
				</div>
			</div>
            <hr>
            <div class="content">
EOF

[ -z "$1" ] || /bin/sh $*

cat << EOF
            </div>
		</div>

	</body>
</html>
EOF
