#!/bin/sh

EDITOR=nvim
TEMPFILE=/tmp/blog_entry.md

$EDITOR $TEMPFILE

NAME=site/entries/$(head -1 $TEMPFILE | cut -d" " -f2- | tr " " "-" | tr "[:upper:]" "[:lower:]").html

cp $TEMPFILE "$NAME"
rm $TEMPFILE

chmod +x $NAME
